import React from "react";

import "./App.css";

/**
 *
 *  Тестовое задание
 *  1) Пофиксить размеры поля ввода, чтобы оно не занимало отступ
 *  2) Получить список элементов при старте приложения, и сохранить его в Redux-хранилище
 *      - https://jsonplaceholder.typicode.com/users
 *
 *  3) Сверстать карточку элемента
 *    - design.png
 *    - Цвета:
 *      E-mail: #aaa
 *      Кнопка: #f33
 *      Границы: #ccc
 *      Тень: 0px 2px 10px 1px rgba(0,0,0,.1)
 *    - Размеры:
 *      Имя, шрифт: 1rem
 *      E-mail, шрифт: 0.875rem
 *      Радиус углов: 4px
 *
 *  4) Отрендерить список элементов
 *  5) Реализовать удаление элементов из Redux-хранилища
 *  6) Отфильтровать выводимые элементы по полю для поиска
 *
 *  Что должно получиться в итоге:
 *    - endresult.png
 *
 */

const App = () => {
  return (
    <div className="container">
      <div className="app">
        <h3 className="app-title">Список элементов</h3>
        <input className="app-search" placeholder="Введите фразу для поиска" />
        <div className="app-elements">
          <p>Здесь должен был быть список элементов... :(</p>
        </div>
      </div>
    </div>
  );
};

export default App;
